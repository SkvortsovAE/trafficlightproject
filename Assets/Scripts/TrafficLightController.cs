﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrafficLightController : MonoBehaviour
{
    public List<Light> Lights;

    public List<MeshRenderer> Headlights;

    public List<int> Intervals; //Интервалы для каждого сигнала 

    public List<int> Gap; //Временные оступы задающие мигание

    public float FlashingTimer = 1; //Частота мигания

    public int LightIntensity = 10; //Интенсивность света

    public bool isWorking = false; //Переменная для проверки работы светофора

    private float CurrentTimer=0;

    private float CurrentFlashingTimer;

    private int k = -1;


    public void CustomReset()
    {
        k = -1;
        CurrentTimer = 0;
        CurrentFlashingTimer = FlashingTimer;
        foreach(var light in Lights)
        {
            light.intensity = 0;
        }
        isWorking = false;
    }

    public string GetColorByCurrentHeadlight()
    {
        if (k>=0)
        {
           return Headlights[k].material.name;
        }
        return "No color signal";
    }

    void Start()
    {
        CurrentFlashingTimer = FlashingTimer;
        for(int i=0; i<Headlights.Count && i<Lights.Count; i++)
        {
            if(Headlights[i]!=null && Lights[i]!=null)
            {
                Lights[i].color = Headlights[i].material.color;
            }
        }
    }
    void FixedUpdate()
    {
        if(isWorking)
        {
            if (CurrentTimer <= 0)
            {
                if (k>=0 && Lights[k].intensity > 0 )
                {
                    Lights[k].intensity = 0;
                }
                if (k != Intervals.Count-1)
                {
                    k++;
                }
                else
                {
                    k = 0;
                }
                CurrentTimer = Intervals[k];
                Lights[k].intensity = Mathf.Abs(10);
                CurrentFlashingTimer = FlashingTimer;
            }
            else
            {
                if (CurrentTimer <= Gap[k])
                {
                    if (CurrentFlashingTimer == FlashingTimer)
                    {
                        if(Lights[k].intensity>0)
                        {
                            Lights[k].intensity = 0;
                        }
                        else
                        {
                            Lights[k].intensity = LightIntensity;
                        }
                        CurrentFlashingTimer = 0;
                    }
                    CurrentFlashingTimer += Time.deltaTime;
                    CurrentFlashingTimer = (float)System.Math.Round(CurrentFlashingTimer, 2);
                }
            }
            CurrentTimer -= Time.deltaTime;
            CurrentTimer = (float)System.Math.Round(CurrentTimer, 2);
        }
    }
}
