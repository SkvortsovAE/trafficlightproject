﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TrafficLightUI : MonoBehaviour
{
    public Button StartButton;

    public Button StopButton;

    public Button ResetButton;

    public TrafficLightController TrafficLightController;

    public Text SceneName;

    public Text CurrentColorName;


    void Start()
    {
        StartButton.onClick.AddListener(() => TrafficLightController.isWorking = true);

        StopButton.onClick.AddListener(() => TrafficLightController.isWorking = false);

        ResetButton.onClick.AddListener(TrafficLightController.CustomReset);

        SceneName.text = SceneManager.GetActiveScene().name;
    }
    void Update()
    {
        CurrentColorName.text = TrafficLightController.GetColorByCurrentHeadlight().Replace("(Instance)", "signal");
    }
}
